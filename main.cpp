#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "mqtt_msg_pub.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));


    mqtt_msg_pub msg_pub;
    QQmlContext* msgpub = engine.rootContext();
    msgpub->setContextProperty("msgpub1",&msg_pub);


    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
