import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.0

Button {
    id: button
    text: qsTr("Led_On")
    Layout.preferredHeight: 40
    Layout.preferredWidth: 203
}
