#ifndef MQTT_MSG_PUB_H
#define MQTT_MSG_PUB_H

#include <QObject>
#include <QtMqtt/QtMqtt>

class mqtt_msg_pub : public QObject
{
    Q_OBJECT
public:
    explicit mqtt_msg_pub(QObject *parent = nullptr);

signals:

public slots:
    void led_on(const QString& value);
    void led_off(const QString& value);
private:
    QMqttClient *msg;

};

#endif // MQTT_MSG_PUB_H
