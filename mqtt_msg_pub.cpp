#include "mqtt_msg_pub.h"
#include "qdebug.h"
#include <QtMqtt/QtMqtt>

QString topic="nielit2";

mqtt_msg_pub::mqtt_msg_pub(QObject *parent) : QObject(parent)
{
    msg = new QMqttClient(this);
    msg->setHostname("iot.eclipse.org");
    msg->setPort(1883);
    msg->connectToHost();
}

void mqtt_msg_pub::led_on(const QString &value)
{
    qDebug() << "led _on value : " << value;
    msg->publish(topic,value.toUtf8(),1);
}

void mqtt_msg_pub::led_off(const QString &value)
{
    qDebug() << "led_off value : " << value;
    msg->publish(topic,value.toUtf8(),1);
}

