import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.0

Window {
    visible: true
    width: 414
    height: 597
    color: "#131111"
    title: qsTr("Hello World")

    Rectangle {
        id: rectangle
        color: "#000000"
        border.width: 3
        anchors.fill: parent
        border.color: "#0e169e"

        GroupBox {
            id: groupBox
            x: 60
            y: 110
            width: 277
            height: 133
            title: qsTr("Group Box")

            ColumnLayout {
                x: 25
                y: 2

                Button {
                    id: button
                    text: qsTr("Led_On")
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 203
                    onClicked: {
                        msgpub1.led_on("1");
                    }
                }

                Button {
                    id: button1
                    text: qsTr("Led_Off")
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 203
                    onClicked: {
                        msgpub1.led_off("0");
                    }
                }
            }
        }
    }
}
